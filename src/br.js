/**
 * @license Copyright (c) 2003-2018, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md.
 */

/**
 * @module paragraph/paragraph
 */

import Plugin from '@ckeditor/ckeditor5-core/src/plugin';
import brIcon from '../theme/icons/br.svg';
import ButtonView from '@ckeditor/ckeditor5-ui/src/button/buttonview';
import EnterCommand from '@ckeditor/ckeditor5-enter/src/entercommand';

/**
 * The paragraph feature for the editor.
 * It introduces the `<paragraph>` element in the model which renders as a `<p>` element in the DOM and data.
 *
 * @extends module:core/plugin~Plugin
 */
export default class Br extends Plugin {
	/**
	 * @inheritDoc
	 */
	static get pluginName() {
		return 'Br';
	}

	/**
	 * @inheritDoc
	 */
    init() {
        const editor = this.editor;
		const t = editor.t;

        editor.ui.componentFactory.add( 'br', locale => {
            const view = new ButtonView( locale );

			editor.commands.add( 'enter', new EnterCommand( editor ) );

            view.set( {
                label: 'Insert br',
                icon: brIcon,
                tooltip: true
            } );

			view.on( 'execute', () => {
				editor.execute( 'enter' );
				editor.editing.view.scrollToTheSelection();
			} );

    //         view.on( 'execute', () => {
				// editor.execute( 'paragraph' );
    //             // editor.model.change( writer => {
    //             //     const brElement = writer.createElement( 'p');
    //             //     editor.model.insertContent( brElement, editor.model.document.selection );
    //             // } );
    //         } );

            return view;
        } );
    }
}
